package com.ecommerce.repository;

import com.ecommerce.modele.Produs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryProdus extends JpaRepository<Produs, Integer> {
}
