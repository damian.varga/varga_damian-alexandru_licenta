package com.ecommerce.repository;

import com.ecommerce.modele.Order;
import com.ecommerce.modele.Utilizator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositoryComanda extends JpaRepository<Order, Integer> {
    List<Order> findAllByUtilizatorOrderByDataCreatDesc(Utilizator utilizator);

}