package com.ecommerce.repository;

import com.ecommerce.modele.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryCategorie extends JpaRepository<Categorie, Integer> {

}
