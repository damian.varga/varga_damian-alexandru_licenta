package com.ecommerce.repository;

import com.ecommerce.modele.Utilizator;
import com.ecommerce.modele.WishList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositoryWishList extends JpaRepository<WishList, Integer> {

    List<WishList> findAllByUtilizatorOrderByDataCreatDesc(Utilizator utilizator);

    WishList findByProdus_IdAndUtilizator_Id(int produsId, int userId);
}