package com.ecommerce.repository;

import com.ecommerce.modele.Utilizator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositoryUtilizator extends JpaRepository<Utilizator, Integer> {
    List<Utilizator> findAll();
    Utilizator findByEmail(String email);
}
