package com.ecommerce.repository;

import com.ecommerce.modele.TokenAutentificare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RepositoryTokenAutentificare extends JpaRepository<TokenAutentificare, Long> {

    Optional<TokenAutentificare> findByToken(String token);
}
