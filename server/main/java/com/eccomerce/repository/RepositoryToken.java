package com.ecommerce.repository;

import com.ecommerce.modele.TokenAutentificare;
import com.ecommerce.modele.Utilizator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositoryToken extends JpaRepository<TokenAutentificare, Integer> {
    TokenAutentificare findTokenByToken(String token);
    TokenAutentificare findByUtilizator(Utilizator utilizator);
}
