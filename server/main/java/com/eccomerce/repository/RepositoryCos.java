package com.ecommerce.repository;

import com.ecommerce.modele.Cos;
import com.ecommerce.modele.Utilizator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RepositoryCos extends JpaRepository<Cos, Integer> {

    List<Cos> findAllByUtilizatorOrderByDataCreatDesc(Utilizator utilizator);

    List<Cos> deleteByUtilizator(Utilizator utilizator);

}