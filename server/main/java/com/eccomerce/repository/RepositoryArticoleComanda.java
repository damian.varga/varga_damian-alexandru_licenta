package com.ecommerce.repository;

import com.ecommerce.modele.ArticolComanda;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryArticoleComanda extends JpaRepository<ArticolComanda,Integer> {
}