package com.ecommerce.exceptii;

public class ExceptiePersonalizata extends IllegalArgumentException {
    public ExceptiePersonalizata(String msg) {
        super(msg);
    }

}
