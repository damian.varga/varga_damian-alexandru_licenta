package com.ecommerce.exceptii;

public class ExceptieProdusNegasit extends IllegalArgumentException {
    public ExceptieProdusNegasit(String msg) {
        super(msg);
    }
}