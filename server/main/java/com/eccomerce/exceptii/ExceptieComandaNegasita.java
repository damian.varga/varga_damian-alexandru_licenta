package com.ecommerce.exceptii;

public class ExceptieComandaNegasita extends IllegalArgumentException {
    public ExceptieComandaNegasita(String msg) {
        super(msg);
    }
}