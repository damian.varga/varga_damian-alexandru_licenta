package com.ecommerce.exceptii;

public class ExceptieArticolCosInexistent extends IllegalArgumentException {
    public ExceptieArticolCosInexistent(String msg) {
        super(msg);
    }
}