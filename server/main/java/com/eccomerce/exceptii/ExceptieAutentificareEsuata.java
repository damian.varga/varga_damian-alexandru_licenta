package com.ecommerce.exceptii;

public class ExceptieAutentificareEsuata extends IllegalArgumentException {
    public ExceptieAutentificareEsuata(String msg){
        super(msg);
    }
}
