package com.ecommerce.exceptii;

public class ExceptieArticolWishlistInexistent extends IllegalArgumentException {
    public ExceptieArticolWishlistInexistent(String msg) {
        super(msg);
    }
}