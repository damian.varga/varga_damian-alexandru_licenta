package com.ecommerce.servicii;

import com.ecommerce.modele.Categorie;
import com.ecommerce.repository.RepositoryCategorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiciuCategorie {

    @Autowired
    RepositoryCategorie repositoryCategorie;

    public void creareCategorie(Categorie categorie) {
        repositoryCategorie.save(categorie);
    }

    public List<Categorie> afisareCategorii() {
        return repositoryCategorie.findAll();
    }

    public void actualizareCategorie(int categoryId, Categorie updateCategorie) {
        Categorie categorie = repositoryCategorie.getById(categoryId);
        categorie.setNumeCategorie(updateCategorie.getNumeCategorie());
        categorie.setDescriere(updateCategorie.getDescriere());
        categorie.setImagineUrl(updateCategorie.getImagineUrl());
        repositoryCategorie.save(categorie);
    }

    public void stergeCategorie(int categoryId) {
        repositoryCategorie.deleteById(categoryId);
    }

    public boolean gasesteDupaId(int categoryId) {
        return repositoryCategorie.findById(categoryId).isPresent();
    }
}
