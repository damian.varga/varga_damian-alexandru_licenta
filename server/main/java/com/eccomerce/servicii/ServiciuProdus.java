package com.ecommerce.servicii;

import com.ecommerce.exceptii.ExceptieProdusNegasit;
import com.ecommerce.dto.DtoProdus;
import com.ecommerce.modele.Categorie;
import com.ecommerce.modele.Produs;
import com.ecommerce.repository.RepositoryProdus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ServiciuProdus {
    @Autowired
    RepositoryProdus repositoryProdus;

    public void creareProdus(DtoProdus dtoProdus, Categorie categorie) {
        Produs produs = new Produs();
        produs.setDescriere(dtoProdus.getDescriere());
        produs.setImagine1URL(dtoProdus.getImagineURL());
        produs.setImagine2URL(dtoProdus.getImagine2URL());
        produs.setImagine3URL(dtoProdus.getImagine3URL());
        produs.setImagine4URL(dtoProdus.getImagine4URL());
        produs.setNume(dtoProdus.getNume());
        produs.setCategory(categorie);
        produs.setPret(dtoProdus.getPret());
        produs.setMarimi(dtoProdus.getMarimi());
        produs.setGen(dtoProdus.getGen());
        repositoryProdus.save(produs);
    }

    public DtoProdus obtineDtoProdus(Produs produs) {
        DtoProdus dtoProdus = new DtoProdus();
        dtoProdus.setDescriere(produs.getDescriere());
        dtoProdus.setImagineURL(produs.getImagine1URL());
        dtoProdus.setImagine2URL(produs.getImagine2URL());
        dtoProdus.setImagine3URL(produs.getImagine3URL());
        dtoProdus.setImagine4URL(produs.getImagine4URL());
        dtoProdus.setNume(produs.getNume());
        dtoProdus.setIdCategorie(produs.getCategory().getId());
        dtoProdus.setPret(produs.getPret());
        dtoProdus.setId(produs.getId());
        dtoProdus.setMarimi(produs.getMarimi());
        dtoProdus.setGen(produs.getGen());
        return dtoProdus;
    }

    public List<DtoProdus> obtineToateProdusele() {
        List<Produs> toateProdusele = repositoryProdus.findAll();

        List<DtoProdus> dtoProduse = new ArrayList<>();
        for(Produs produs: toateProdusele) {
            dtoProduse.add(obtineDtoProdus(produs));
        }
        return dtoProduse;
    }

    public void actualizareProdus(DtoProdus dtoProdus, Integer productId) throws Exception {
        Optional<Produs> optionalProduct = repositoryProdus.findById(productId);
        if (!optionalProduct.isPresent()) {
            throw new Exception("produsul nu este prezent");
        }
        Produs produs = optionalProduct.get();
        produs.setDescriere(dtoProdus.getDescriere());
        produs.setImagine1URL(dtoProdus.getImagineURL());
        produs.setImagine2URL(dtoProdus.getImagine2URL());
        produs.setImagine3URL(dtoProdus.getImagine3URL());
        produs.setImagine4URL(dtoProdus.getImagine4URL());
        produs.setNume(dtoProdus.getNume());
        produs.setPret(dtoProdus.getPret());
        produs.setMarimi(dtoProdus.getMarimi());
        produs.setGen(dtoProdus.getGen());
        repositoryProdus.save(produs);
    }

    public Produs obtineProdusDupaId(Integer IdProdus) throws ExceptieProdusNegasit {
        Optional<Produs> optionalProduct = repositoryProdus.findById(IdProdus);
        if (!optionalProduct.isPresent())
            throw new ExceptieProdusNegasit("Id produs invalid " + IdProdus);
        return optionalProduct.get();
    }

    public void stergereProdus(Integer IdProdus) throws ExceptieProdusNegasit {
        if (!repositoryProdus.existsById(IdProdus)) {
            throw new ExceptieProdusNegasit("Produsul cu Id-ul " + IdProdus + " nu exista.");
        }
        repositoryProdus.deleteById(IdProdus);
    }
}
