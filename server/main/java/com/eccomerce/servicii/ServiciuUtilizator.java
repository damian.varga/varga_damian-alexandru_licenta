package com.ecommerce.servicii;

import com.ecommerce.repository.RepositoryUtilizator;
import com.ecommerce.dto.DtoRaspuns;
import com.ecommerce.dto.utilizator.DtoLogare;
import com.ecommerce.dto.utilizator.DtoRaspunsLogare;
import com.ecommerce.dto.utilizator.DtoInregistrare;
import com.ecommerce.exceptii.ExceptieAutentificareEsuata;
import com.ecommerce.exceptii.ExceptiePersonalizata;
import com.ecommerce.modele.TokenAutentificare;
import com.ecommerce.modele.Utilizator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

@Service
public class ServiciuUtilizator {

    @Autowired
    RepositoryUtilizator repositoryUtilizator;

    @Autowired
    ServiciuAutentificare serviciuAutentificare;

    @Transactional
    public DtoRaspuns inregistrare(DtoInregistrare dtoInregistrare) {
        if (Objects.nonNull(repositoryUtilizator.findByEmail(dtoInregistrare.getEmail()))) {
            throw new ExceptiePersonalizata("utilizator existent");
        }

        String encryptedpassword = dtoInregistrare.getParola();

        try {
            encryptedpassword = ascundeParola(dtoInregistrare.getParola());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        Utilizator utilizator = new Utilizator(dtoInregistrare.getNumeFamilie(), dtoInregistrare.getPrenume(),
                dtoInregistrare.getEmail(), encryptedpassword, dtoInregistrare.isPermisiuni());
        repositoryUtilizator.save(utilizator);
        final TokenAutentificare authenticationToken = new TokenAutentificare(utilizator);
        serviciuAutentificare.salvareTokenConfirmare(authenticationToken);
        DtoRaspuns dtoRaspuns = new DtoRaspuns("success", "utilizator creat cu succes");
        return dtoRaspuns;
    }

    private String ascundeParola(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        String hash = DatatypeConverter
                .printHexBinary(digest).toUpperCase();
        return hash;
    }

    public DtoRaspunsLogare logare(DtoLogare dtoLogare) {

        Utilizator utilizator = repositoryUtilizator.findByEmail(dtoLogare.getEmail());

        if (Objects.isNull(utilizator)) {
            throw new ExceptieAutentificareEsuata("utilizator invalid");
        }

        try {
            if (!utilizator.getParola().equals(ascundeParola(dtoLogare.getParola()))) {
                throw new ExceptieAutentificareEsuata("parola gresita");
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        TokenAutentificare token = serviciuAutentificare.getToken(utilizator);

        if (Objects.isNull(token)) {
            throw new ExceptiePersonalizata("token-ul nu este prezent");
        }

        return new DtoRaspunsLogare("success", token.getToken());

    }
}
