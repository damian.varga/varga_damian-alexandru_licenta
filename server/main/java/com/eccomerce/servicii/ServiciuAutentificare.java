package com.ecommerce.servicii;

import com.ecommerce.exceptii.ExceptieAutentificareEsuata;
import com.ecommerce.repository.RepositoryToken;
import com.ecommerce.repository.RepositoryTokenAutentificare;
import com.ecommerce.config.MessageStrings;
import com.ecommerce.modele.TokenAutentificare;
import com.ecommerce.modele.Utilizator;
import com.ecommerce.utils.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ServiciuAutentificare {

    @Autowired
    RepositoryToken repository;
    @Autowired
    RepositoryToken repositoryToken;

    @Autowired
    RepositoryTokenAutentificare repositoryTokenAutentificare;

    public void salvareTokenConfirmare(TokenAutentificare authenticationToken) {
        repositoryToken.save(authenticationToken);
    }
    public TokenAutentificare getToken(Utilizator utilizator) {
        return repositoryToken.findByUtilizator(utilizator);
    }

    public Utilizator obtineUtilizator(String token) {
        TokenAutentificare authenticationToken = repository.findTokenByToken(token);
        if (Helper.notNull(authenticationToken)) {
            if (Helper.notNull(authenticationToken.getUser())) {
                return authenticationToken.getUser();
            }
        }
        return null;
    }

    public Utilizator autentificare(String token) throws ExceptieAutentificareEsuata {
        if (!Helper.notNull(token)) {
            throw new ExceptieAutentificareEsuata(MessageStrings.AUTH_TOEKN_NOT_PRESENT);
        }
        if (!Helper.notNull(obtineUtilizator(token))) {
            throw new ExceptieAutentificareEsuata(MessageStrings.AUTH_TOEKN_NOT_VALID);
        }
        return null;
    }
    public Utilizator obtineUtilizatorDupaToken(String token) throws ExceptieAutentificareEsuata {
        Optional<TokenAutentificare> TokenOptional = repositoryTokenAutentificare.findByToken(token);

        if (TokenOptional.isPresent()) {
            TokenAutentificare authenticationToken = TokenOptional.get();
                return authenticationToken.getUser();
        } else {
            throw new ExceptieAutentificareEsuata("Token invalid");
        }
    }
}
