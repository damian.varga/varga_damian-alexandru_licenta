package com.ecommerce.servicii;

import com.ecommerce.repository.RepositoryArticoleComanda;
import com.ecommerce.modele.ArticolComanda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ServiciuArticoleComanda {

    @Autowired
    private RepositoryArticoleComanda repositoryArticoleComanda;

    public void addOrderedProducts(ArticolComanda articolComanda) {
        repositoryArticoleComanda.save(articolComanda);
    }


}