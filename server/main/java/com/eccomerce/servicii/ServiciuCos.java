package com.ecommerce.servicii;

import com.ecommerce.modele.Cos;
import com.ecommerce.modele.Produs;
import com.ecommerce.modele.Utilizator;
import com.ecommerce.repository.RepositoryCos;
import com.ecommerce.dto.cos.DtoAdaugaLaCos;
import com.ecommerce.dto.cos.DtoCos;
import com.ecommerce.dto.cos.DtoArticolCos;
import com.ecommerce.exceptii.ExceptieArticolCosInexistent;
import com.ecommerce.modele.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ServiciuCos {
    @Autowired
    private RepositoryCos repositoryCos;
    public ServiciuCos(){}
    public ServiciuCos(RepositoryCos repositoryCos) {
        this.repositoryCos = repositoryCos;
    }

    public void adaugaLaCos(DtoAdaugaLaCos dtoAdaugaLaCos, Produs product, Utilizator utilizator){
        Cos cos = new Cos(product, dtoAdaugaLaCos.getCantitate(), dtoAdaugaLaCos.getMarime(), utilizator);
        repositoryCos.save(cos);
    }

    public DtoCos listaArticoleCos(Utilizator utilizator) {
        List<Cos> cosList = repositoryCos.findAllByUtilizatorOrderByDataCreatDesc(utilizator);
        List<DtoArticolCos> cartItems = new ArrayList<>();
        for (Cos cos : cosList){
            DtoArticolCos dtoArticolCos = obtineDtoDupaCos(cos);
            cartItems.add(dtoArticolCos);
        }
        double totalCost = 0;
        for (DtoArticolCos dtoArticolCos :cartItems){
            totalCost += (dtoArticolCos.getProdus().getPret()* dtoArticolCos.getCantitate());
        }
        return new DtoCos(cartItems,totalCost);
    }

    public static DtoArticolCos obtineDtoDupaCos(Cos cos) {
        return new DtoArticolCos(cos);
    }


    public void actualizareArticolCos(DtoAdaugaLaCos cartDto, Utilizator utilizator, Produs product){
        Cos cos = repositoryCos.getOne(cartDto.getId());
        cos.setCantitate(cartDto.getCantitate());
        cos.setMarime(cartDto.getMarime());
        cos.setDataCreat(new Date());
        repositoryCos.save(cos);
    }

    public void stergereArticolCos(int id, int userId) throws ExceptieArticolCosInexistent {
        if (!repositoryCos.existsById(id))
            throw new ExceptieArticolCosInexistent("Id cos invalid : " + id);
        repositoryCos.deleteById(id);

    }
    public void stergereArticoleCosDupaUtilizator(Utilizator utilizator) {
        repositoryCos.deleteByUtilizator(utilizator);
    }
}
