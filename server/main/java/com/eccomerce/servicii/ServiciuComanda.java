package com.ecommerce.servicii;

import com.ecommerce.repository.RepositoryArticoleComanda;
import com.ecommerce.repository.RepositoryComanda;
import com.ecommerce.modele.Utilizator;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import com.ecommerce.dto.cos.DtoCos;
import com.ecommerce.dto.cos.DtoArticolCos;
import com.ecommerce.dto.checkout.DtoArticolCheckout;
import com.ecommerce.exceptii.ExceptieComandaNegasita;
import com.ecommerce.modele.Order;
import com.ecommerce.modele.ArticolComanda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class ServiciuComanda {

    @Autowired
    private ServiciuCos serviciuCos;

    @Autowired
    RepositoryComanda repositoryComanda;

    @Autowired
    RepositoryArticoleComanda repositoryArticoleComanda;

    @Value("${BASE_URL}")
    private String baseURL;

    @Value("${STRIPE_SECRET_KEY}")
    private String apiKey;

    public void stergereComanda(Integer orderId) throws ExceptieComandaNegasita {
        Order order = repositoryComanda.findById(orderId)
                .orElseThrow(() -> new ExceptieComandaNegasita("Comanda nu a fost gasita"));

        repositoryComanda.delete(order);
    }



    private SessionCreateParams.LineItem.PriceData creazaDatePret(DtoArticolCheckout dtoArticolCheckout) {
        return SessionCreateParams.LineItem.PriceData.builder()
                .setCurrency("usd")
                .setUnitAmount((long)(dtoArticolCheckout.getPret()*100))
                .setProductData(
                        SessionCreateParams.LineItem.PriceData.ProductData.builder()
                                .setName(dtoArticolCheckout.getNumeProdus())
                                .build())
                .build();
    }

    private SessionCreateParams.LineItem creazaSesiune(DtoArticolCheckout dtoArticolCheckout) {
        return SessionCreateParams.LineItem.builder()
                .setPriceData(creazaDatePret(dtoArticolCheckout))
                .setQuantity(Long.parseLong(String.valueOf(dtoArticolCheckout.getCantitate())))
                .build();
    }
    public Session creareSesiune(List<DtoArticolCheckout> dtoArticolCheckoutList) throws StripeException {

        String successURL = baseURL + "payment/success";
        String failedURL = baseURL + "payment/failed";
        Stripe.apiKey = apiKey;
        List<SessionCreateParams.LineItem> sessionItemsList = new ArrayList<>();

        for (DtoArticolCheckout dtoArticolCheckout : dtoArticolCheckoutList) {
            sessionItemsList.add(creazaSesiune(dtoArticolCheckout));
        }
        SessionCreateParams params = SessionCreateParams.builder()
                .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
                .setMode(SessionCreateParams.Mode.PAYMENT)
                .setCancelUrl(failedURL)
                .setSuccessUrl(successURL)
                .addAllLineItem(sessionItemsList)
                .build();
        return Session.create(params);
    }

    public void plasareComanda(Utilizator utilizator, String sessionId) {
        DtoCos dtoCos = serviciuCos.listaArticoleCos(utilizator);
        List<DtoArticolCos> dtoArticolCosList = dtoCos.getcartItems();

        Order newOrder = new Order();
        newOrder.setDataCreat(new Date());
        newOrder.setSessionId(sessionId);
        newOrder.setUser(utilizator);
        newOrder.setPretTotal(dtoCos.getCostTotal());
        repositoryComanda.save(newOrder);
        for (DtoArticolCos dtoArticolCos : dtoArticolCosList) {
            ArticolComanda articolComanda = new ArticolComanda();
            articolComanda.setDataCreat(new Date());
            articolComanda.setPret(dtoArticolCos.getProdus().getPret());
            articolComanda.setProdus(dtoArticolCos.getProdus());
            articolComanda.setCantitate(dtoArticolCos.getCantitate());
            articolComanda.setOrder(newOrder);
            repositoryArticoleComanda.save(articolComanda);
        }
        serviciuCos.stergereArticoleCosDupaUtilizator(utilizator);
    }

    public List<Order> listaComenzi(Utilizator utilizator) {
        return repositoryComanda.findAllByUtilizatorOrderByDataCreatDesc(utilizator);
    }

    public Order obtineComanda(Integer orderId) throws ExceptieComandaNegasita {
        Optional<Order> order = repositoryComanda.findById(orderId);
        if (order.isPresent()) {
            return order.get();
        }
        throw new ExceptieComandaNegasita("Comanda nu este gasita");
    }
}
