package com.ecommerce.servicii;

import com.ecommerce.dto.DtoProdus;
import com.ecommerce.exceptii.ExceptieArticolWishlistInexistent;
import com.ecommerce.modele.Utilizator;
import com.ecommerce.modele.WishList;
import com.ecommerce.repository.RepositoryWishList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiciuWishlist {

    @Autowired
    RepositoryWishList repositoryWishList;

    @Autowired
    ServiciuProdus serviciuProdus;

    public void creareWishlist(WishList wishList) {
        repositoryWishList.save(wishList);
    }

    public List<DtoProdus> obtineWishlistPentruUtilizator(Utilizator utilizator) {
        final List<WishList> wishLists = repositoryWishList.findAllByUtilizatorOrderByDataCreatDesc(utilizator);
        List<DtoProdus> dtoProduse = new ArrayList<>();
        for (WishList wishList: wishLists) {
            dtoProduse.add(serviciuProdus.obtineDtoProdus(wishList.getProdus()));
        }

        return dtoProduse;
    }

    public void sergeArticolWishlistDupaIdProdus(int produsId, int userId) throws ExceptieArticolWishlistInexistent {
        WishList wishList = repositoryWishList.findByProdus_IdAndUtilizator_Id(produsId, userId);
        if (wishList == null)
            throw new ExceptieArticolWishlistInexistent("Articolul din wishlist nu a fost gasit pentru produsul cu ID-ul: " + produsId + " si utilizatorul cu ID-ul: " + userId);

        repositoryWishList.delete(wishList);
    }
}
