package com.ecommerce.controller;

import com.ecommerce.modele.Produs;
import com.ecommerce.modele.Utilizator;
import com.ecommerce.servicii.ServiciuCos;
import com.ecommerce.common.ApiResponse;
import com.ecommerce.dto.cos.DtoAdaugaLaCos;
import com.ecommerce.dto.cos.DtoCos;
import com.ecommerce.exceptii.ExceptieAutentificareEsuata;
import com.ecommerce.exceptii.ExceptieArticolCosInexistent;
import com.ecommerce.exceptii.ExceptieProdusNegasit;
import com.ecommerce.modele.*;
import com.ecommerce.servicii.ServiciuAutentificare;
import com.ecommerce.servicii.ServiciuProdus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cart")
public class ControllerCos {
    @Autowired
    private ServiciuCos serviciuCos;

    @Autowired
    private ServiciuProdus serviciuProdus;

    @Autowired
    private ServiciuAutentificare serviciuAutentificare;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse> adaugaLaCos(@RequestBody DtoAdaugaLaCos dtoAdaugaLaCos,
                                                 @RequestParam("token") String token) throws ExceptieAutentificareEsuata, ExceptieProdusNegasit {
        serviciuAutentificare.autentificare(token);
        Utilizator utilizator = serviciuAutentificare.obtineUtilizator(token);
        Produs produs = serviciuProdus.obtineProdusDupaId(dtoAdaugaLaCos.getIdProdus());
        System.out.println("produs de adaugat"+  produs.getNume());
        serviciuCos.adaugaLaCos(dtoAdaugaLaCos, produs, utilizator);
        return new ResponseEntity<ApiResponse>(new ApiResponse(true, "Adaugat la cos"), HttpStatus.CREATED);

    }

    @GetMapping("/")
    public ResponseEntity<DtoCos> obtineArticoleCos(@RequestParam("token") String token) throws ExceptieAutentificareEsuata {
        serviciuAutentificare.autentificare(token);
        Utilizator utilizator = serviciuAutentificare.obtineUtilizator(token);
        DtoCos dtoCos = serviciuCos.listaArticoleCos(utilizator);
        return new ResponseEntity<DtoCos>(dtoCos,HttpStatus.OK);
    }

    @PutMapping("/update/{cartItemId}")
    public ResponseEntity<ApiResponse> actualizareArticolCos(@RequestBody @Valid DtoAdaugaLaCos cartDto,
                                                      @RequestParam("token") String token) throws ExceptieAutentificareEsuata, ExceptieProdusNegasit {
        serviciuAutentificare.autentificare(token);
        Utilizator utilizator = serviciuAutentificare.obtineUtilizator(token);
        Produs produs = serviciuProdus.obtineProdusDupaId(cartDto.getIdProdus());
        serviciuCos.actualizareArticolCos(cartDto, utilizator,produs);
        return new ResponseEntity<ApiResponse>(new ApiResponse(true, "Produsul a fost actualizat"), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{cartItemId}")
    public ResponseEntity<ApiResponse> stergereArticolCos(@PathVariable("cartItemId") int itemID,@RequestParam("token") String token) throws ExceptieAutentificareEsuata, ExceptieArticolCosInexistent {
        serviciuAutentificare.autentificare(token);
        int userId = serviciuAutentificare.obtineUtilizator(token).getId();
        serviciuCos.stergereArticolCos(itemID, userId);
        return new ResponseEntity<ApiResponse>(new ApiResponse(true, "Articolul a fost sters"), HttpStatus.OK);
    }
}
