package com.ecommerce.controller;

import com.ecommerce.repository.RepositoryUtilizator;
import com.ecommerce.dto.DtoRaspuns;
import com.ecommerce.dto.utilizator.DtoLogare;
import com.ecommerce.dto.utilizator.DtoRaspunsLogare;
import com.ecommerce.dto.utilizator.DtoInregistrare;
import com.ecommerce.exceptii.ExceptieAutentificareEsuata;
import com.ecommerce.modele.Utilizator;
import com.ecommerce.servicii.ServiciuAutentificare;
import com.ecommerce.servicii.ServiciuUtilizator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("user")
@RestController
public class ControllerUtilizator {
    @Autowired
    RepositoryUtilizator repositoryUtilizator;
    @Autowired
    ServiciuUtilizator serviciuUtilizator;
    @Autowired
    ServiciuAutentificare serviciuAutentificare;

    @GetMapping("/details")
    public Utilizator obtineDetaliiUtilizator(@RequestParam("token") String token) throws ExceptieAutentificareEsuata {
        serviciuAutentificare.autentificare(token);
        Utilizator utilizator = serviciuAutentificare.obtineUtilizatorDupaToken(token);
        if (utilizator == null) {
            throw new ExceptieAutentificareEsuata("Utilizatorul nu a fost gasit");
        }

        return utilizator;
    }

    @GetMapping("/all")
    public List<Utilizator> gasesteTotiUtilizatorii(@RequestParam("token") String token) throws ExceptieAutentificareEsuata {
        serviciuAutentificare.autentificare(token);
        if (repositoryUtilizator == null) {
            throw new RuntimeException("error");
        }
        return repositoryUtilizator.findAll();
    }

    @PostMapping("/signup")
    public DtoRaspuns inregistrare(@RequestBody DtoInregistrare dtoInregistrare) {
        return serviciuUtilizator.inregistrare(dtoInregistrare);
    }

    @PostMapping("/signin")
    public DtoRaspunsLogare logare(@RequestBody DtoLogare dtoLogare) {
        return serviciuUtilizator.logare(dtoLogare);
    }
}
