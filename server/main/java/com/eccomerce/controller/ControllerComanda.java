package com.ecommerce.controller;

import com.ecommerce.servicii.ServiciuComanda;
import com.ecommerce.modele.Utilizator;
import com.stripe.exception.StripeException;
import com.stripe.model.checkout.Session;
import com.ecommerce.common.ApiResponse;
import com.ecommerce.dto.checkout.DtoArticolCheckout;
import com.ecommerce.dto.checkout.DtoRaspunsStripe;
import com.ecommerce.exceptii.ExceptieAutentificareEsuata;
import com.ecommerce.exceptii.ExceptieComandaNegasita;
import com.ecommerce.modele.Order;
import com.ecommerce.servicii.ServiciuAutentificare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class ControllerComanda {
    @Autowired
    private ServiciuComanda serviciuComanda;

    @Autowired
    private ServiciuAutentificare serviciuAutentificare;

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> stergereComanda(@PathVariable("id") Integer id, @RequestParam("token") String token) {
        try {
            serviciuAutentificare.autentificare(token);
            serviciuComanda.stergereComanda(id);
            return new ResponseEntity<>("Comanda a fost stearsa cu succes", HttpStatus.OK);
        } catch (ExceptieAutentificareEsuata e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        } catch (ExceptieComandaNegasita e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create-checkout-session")
    public ResponseEntity<DtoRaspunsStripe> listaCheckout(@RequestBody List<DtoArticolCheckout> dtoArticolCheckoutList)
            throws StripeException {
        Session session = serviciuComanda.creareSesiune(dtoArticolCheckoutList);
        DtoRaspunsStripe dtoRaspunsStripe = new DtoRaspunsStripe(session.getId());
        return new ResponseEntity<>(dtoRaspunsStripe, HttpStatus.OK);
    }


    @PostMapping("/add")
    public ResponseEntity<ApiResponse> plasareComanda(@RequestParam("token") String token, @RequestParam("sessionId") String sessionId)
            throws ExceptieAutentificareEsuata {
        serviciuAutentificare.autentificare(token);
        Utilizator utilizator = serviciuAutentificare.obtineUtilizator(token);
        serviciuComanda.plasareComanda(utilizator, sessionId);
        return new ResponseEntity<>(new ApiResponse(true, "Comanda a fost plasata"), HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<List<Order>> obtineToateComenzile(@RequestParam("token") String token) throws ExceptieAutentificareEsuata {

        serviciuAutentificare.autentificare(token);
        Utilizator user = serviciuAutentificare.obtineUtilizator(token);
        List<Order> orderDtoList = serviciuComanda.listaComenzi(user);

        return new ResponseEntity<>(orderDtoList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> obtineComandaDupaId(@PathVariable("id") Integer id, @RequestParam("token") String token)
            throws ExceptieAutentificareEsuata {
        serviciuAutentificare.autentificare(token);
        try {
            Order order = serviciuComanda.obtineComanda(id);
            return new ResponseEntity<>(order,HttpStatus.OK);
        }
        catch (ExceptieComandaNegasita e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }

    }
}
