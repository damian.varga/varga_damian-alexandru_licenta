package com.ecommerce.controller;

import com.ecommerce.servicii.ServiciuCategorie;
import com.ecommerce.common.ApiResponse;
import com.ecommerce.modele.Categorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class ControllerCategorie {

    @Autowired
    ServiciuCategorie serviciuCategorie;

    @PostMapping("/create")
    public ResponseEntity<ApiResponse> creareCategorie(@RequestBody Categorie categorie) {
        serviciuCategorie.creareCategorie(categorie);
        return new ResponseEntity<>(new ApiResponse(true, "o noua categorie creata"), HttpStatus.CREATED);
    }

    @GetMapping("/list")
    public List<Categorie> afisareCategorii() {
        return serviciuCategorie.afisareCategorii();
    }

    @PostMapping("/update/{categoryId}")
    public ResponseEntity<ApiResponse> actualizareCategorie(@PathVariable("categoryId") int IdCategorie, @RequestBody Categorie categorie) {
        if (!serviciuCategorie.gasesteDupaId(IdCategorie)) {
            return new ResponseEntity<ApiResponse>(new ApiResponse(false, "categoria nu exista"), HttpStatus.NOT_FOUND);
        }
        serviciuCategorie.actualizareCategorie(IdCategorie, categorie);
        return new ResponseEntity<ApiResponse>(new ApiResponse(true, "categoria a fost actualizata"), HttpStatus.OK);
    }

    @DeleteMapping("/remove/{categoryId}")
    public ResponseEntity<ApiResponse> stergereCategorie(@PathVariable("categoryId") int IdCategorie) {
        if (!serviciuCategorie.gasesteDupaId(IdCategorie)) {
            return new ResponseEntity<>(new ApiResponse(false, "categoria nu exista"), HttpStatus.NOT_FOUND);
        }
        serviciuCategorie.stergeCategorie(IdCategorie);
        return new ResponseEntity<>(new ApiResponse(true, "categoria a fost stearsa"), HttpStatus.OK);
    }
}
