package com.ecommerce.controller;

import com.ecommerce.common.ApiResponse;
import com.ecommerce.dto.DtoProdus;
import com.ecommerce.modele.Categorie;
import com.ecommerce.repository.RepositoryCategorie;
import com.ecommerce.servicii.ServiciuProdus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/product")
public class ControllerProdus {
    @Autowired
    ServiciuProdus serviciuProdus;

    @Autowired
    RepositoryCategorie repositoryCategorie;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse> creareProdus(@RequestBody DtoProdus dtoProdus) {
         Optional<Categorie> optionalCategory = repositoryCategorie.findById(dtoProdus.getIdCategorie());
         if (!optionalCategory.isPresent()) {
             return new ResponseEntity<ApiResponse>(new ApiResponse(false, "categoia nu exista"), HttpStatus.BAD_REQUEST);
         }
         serviciuProdus.creareProdus(dtoProdus, optionalCategory.get());
         return new ResponseEntity<ApiResponse>(new ApiResponse(true, "produsul a fost adaugat"), HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<List<DtoProdus>> obtineProduse() {
        List<DtoProdus> products = serviciuProdus.obtineToateProdusele();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @PostMapping("/update/{productId}")
    public ResponseEntity<ApiResponse> actualizareProdus(@PathVariable("productId") Integer productId, @RequestBody DtoProdus dtoProdus) throws Exception {
        Optional<Categorie> optionalCategory = repositoryCategorie.findById(dtoProdus.getIdCategorie());
        if (!optionalCategory.isPresent()) {
            return new ResponseEntity<ApiResponse>(new ApiResponse(false, "categoria nu exista"), HttpStatus.BAD_REQUEST);
        }
        serviciuProdus.actualizareProdus(dtoProdus, productId);
        return new ResponseEntity<ApiResponse>(new ApiResponse(true, "produsul a fost actualizat"), HttpStatus.OK);
    }

    @DeleteMapping("/remove/{productId}")
    public ResponseEntity<ApiResponse> stergereProdus(@PathVariable("productId") Integer productId) {
        try {
            serviciuProdus.stergereProdus(productId);
            return new ResponseEntity<>(new ApiResponse(true, "Produsul a fost sters"), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ApiResponse(false, "Eroare la stergere produs: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
