package com.ecommerce.controller;

import com.ecommerce.common.ApiResponse;
import com.ecommerce.dto.DtoProdus;
import com.ecommerce.exceptii.ExceptieAutentificareEsuata;
import com.ecommerce.exceptii.ExceptieArticolCosInexistent;
import com.ecommerce.modele.Produs;
import com.ecommerce.modele.Utilizator;
import com.ecommerce.modele.WishList;
import com.ecommerce.servicii.ServiciuAutentificare;
import com.ecommerce.servicii.ServiciuWishlist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/wishlist")
public class ControllerWishlist {

    @Autowired
    ServiciuWishlist serviciuWishlist;

    @Autowired
    ServiciuAutentificare serviciuAutentificare;

    @PostMapping("/add")
    public ResponseEntity<ApiResponse> adaugaLaWishlist(@RequestBody Produs produs,
                                                     @RequestParam("token") String token) {
        serviciuAutentificare.autentificare(token);
        Utilizator utilizator = serviciuAutentificare.obtineUtilizator(token);
        WishList wishList = new WishList(utilizator, produs);
        serviciuWishlist.creareWishlist(wishList);
        ApiResponse apiResponse = new ApiResponse(true, "Adaugat la wishlist");
        return  new ResponseEntity<>(apiResponse, HttpStatus.CREATED);

    }

    @DeleteMapping("/delete/{productId}/")
    public ResponseEntity<ApiResponse> stergeArticolDinWishlist(@PathVariable("productId") int itemID,@RequestParam("token") String token) throws ExceptieAutentificareEsuata, ExceptieArticolCosInexistent {
        serviciuAutentificare.autentificare(token);
        int userId = serviciuAutentificare.obtineUtilizator(token).getId();
        serviciuWishlist.sergeArticolWishlistDupaIdProdus(itemID, userId);
        return new ResponseEntity<ApiResponse>(new ApiResponse(true, "Articolul a fost sters"), HttpStatus.OK);
    }

    @GetMapping("/{token}")
    public ResponseEntity<List<DtoProdus>> obtineWishlist(@PathVariable("token") String token) {
        serviciuAutentificare.autentificare(token);
        Utilizator utilizator = serviciuAutentificare.obtineUtilizator(token);
        List<DtoProdus> dtoProduses = serviciuWishlist.obtineWishlistPentruUtilizator(utilizator);
        return new ResponseEntity<>(dtoProduses, HttpStatus.OK);
    }
}
