package com.ecommerce.modele;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "articolecomanda")
public class ArticolComanda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(name = "cantitate")
    private @NotNull int cantitate;

    @Column(name = "pret")
    private @NotNull double pret;


    @Column(name = "data_creat")
    private Date dataCreat;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "comanda_id", referencedColumnName = "id")
    private Order order;

    @OneToOne
    @JoinColumn(name = "produs_id", referencedColumnName = "id")
    private Produs produs;

    public ArticolComanda(){}

    public ArticolComanda(Order order, @NotNull Produs produs, @NotNull int cantitate, @NotNull double pret) {
        this.produs = produs;
        this.cantitate = cantitate;
        this.pret = pret;
        this.order = order;
        this.dataCreat = new Date();
    }

    public Produs getProdus() {
        return produs;
    }

    public void setProdus(Produs produs) {
        this.produs = produs;
    }


    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public double getPret() {
        return pret;
    }

    public void setPret(double pret) {
        this.pret = pret;
    }

    public Date getDataCreat() {
        return dataCreat;
    }

    public void setDataCreat(Date dataCreat) {
        this.dataCreat = dataCreat;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}