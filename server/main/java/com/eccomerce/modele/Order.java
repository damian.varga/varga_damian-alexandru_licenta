package com.ecommerce.modele;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="comenzi")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(name = "data_creat")
    private Date dataCreat;

    @Column(name = "pret_total")
    private Double pretTotal;

    @Column(name = "sesiune_id")
    private String sessionId;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ArticolComanda> articolComanda;

    @ManyToOne()
    @JsonIgnore
    @JoinColumn(name = "utilizator_id", referencedColumnName = "id")
    private Utilizator utilizator;

    public Order() {
    }


    public List<ArticolComanda> getOrderItems() {
        return articolComanda;
    }

    public void setOrderItems(List<ArticolComanda> articolComanda) {
        this.articolComanda = articolComanda;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Date getDataCreat() {
        return dataCreat;
    }

    public void setDataCreat(Date dataCreat) {
        this.dataCreat = dataCreat;
    }

    public Double getPretTotal() {
        return pretTotal;
    }

    public void setPretTotal(Double pretTotal) {
        this.pretTotal = pretTotal;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Utilizator getUser() {
        return utilizator;
    }

    public void setUser(Utilizator utilizator) {
        this.utilizator = utilizator;
    }
}