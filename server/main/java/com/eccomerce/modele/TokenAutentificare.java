package com.ecommerce.modele;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "tokens")
public class TokenAutentificare {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    private String token;

    @Column(name = "data_creat")
    private Date dataCreat;

    @OneToOne(targetEntity = Utilizator.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "utilizator_id")
    private Utilizator utilizator;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getDataCreat() {
        return dataCreat;
    }

    public void setDataCreat(Date dataCreat) {
        this.dataCreat = dataCreat;
    }

    public Utilizator getUser() {
        return utilizator;
    }

    public void setUser(Utilizator utilizator) {
        this.utilizator = utilizator;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TokenAutentificare(Utilizator utilizator) {
        this.utilizator = utilizator;
        this.dataCreat = new Date();
        this.token = UUID.randomUUID().toString();
    }

    public TokenAutentificare() {
    }
}
