package com.ecommerce.modele;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "wishlist")
public class WishList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(targetEntity = Utilizator.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "utilizator_id")
    private Utilizator utilizator;

    @Column(name = "data_creat")
    private Date dataCreat;

    @ManyToOne
    @JoinColumn(name = "produs_id")
    private Produs produs;

    public WishList() {
    }

    public WishList(Integer id, Utilizator utilizator, Date dataCreat, Produs produs) {
        this.id = id;
        this.utilizator = utilizator;
        this.dataCreat = dataCreat;
        this.produs = produs;
    }

    public WishList(Utilizator utilizator, Produs produs) {
        this.utilizator = utilizator;
        this.produs = produs;
        this.dataCreat = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Utilizator getUser() {
        return utilizator;
    }

    public void setUser(Utilizator utilizator) {
        this.utilizator = utilizator;
    }

    public Date getDataCreat() {
        return dataCreat;
    }

    public void setDataCreat(Date dataCreat) {
        this.dataCreat = dataCreat;
    }

    public Produs getProdus() {
        return produs;
    }

    public void setProdus(Produs produs) {
        this.produs = produs;
    }
}