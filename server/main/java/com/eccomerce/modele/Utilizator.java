package com.ecommerce.modele;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "utilizatori")
public class Utilizator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nume_familie")
    private String numeFamilie;

    @Column(name = "prenume")
    private String prenume;

    @Column(name = "email")
    private String email;

    @Column(name = "parola")
    private String parola;

    @Column(name = "permisiuni")
    private boolean permisiuni;

    public boolean isPermisiuni() {
        return permisiuni;
    }

    public void setPermisiuni(boolean permisiuni) {
        this.permisiuni = permisiuni;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumeFamilie() {
        return numeFamilie;
    }

    public void setNumeFamilie(String numeFamilie) {
        this.numeFamilie = numeFamilie;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public Utilizator(String numeFamilie, String prenume, String email, String parola, boolean permisiuni) {
        this.numeFamilie = numeFamilie;
        this.prenume = prenume;
        this.email = email;
        this.parola = parola;
        this.permisiuni = permisiuni;
    }

    public Utilizator() {
    }
}
