package com.ecommerce.modele;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "produse")
public class Produs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private @NotNull String nume;
    private @NotNull String imagine1URL;

    private String imagine2URL;

    private String imagine3URL;

    private String imagine4URL;
    private @NotNull double pret;
    private @NotNull String descriere;

    @Column(name = "sizes")
    private String marimi;

    private String gen;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "id_categorie")
    Categorie categorie;

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public String getMarimi() {
        return marimi;
    }

    public void setMarimi(String marimi) {
        this.marimi = marimi;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getImagine1URL() {
        return imagine1URL;
    }

    public void setImagine1URL(String imagine1URL) {
        this.imagine1URL = imagine1URL;
    }

    public String getImagine2URL() {
        return imagine2URL;
    }

    public void setImagine2URL(String imagine2URL) {
        this.imagine2URL = imagine2URL;
    }

    public String getImagine3URL() {
        return imagine3URL;
    }

    public void setImagine3URL(String imagine3URL) {
        this.imagine3URL = imagine3URL;
    }

    public String getImagine4URL() {
        return imagine4URL;
    }

    public void setImagine4URL(String imagine4URL) {
        this.imagine4URL = imagine4URL;
    }

    public double getPret() {
        return pret;
    }

    public void setPret(double pret) {
        this.pret = pret;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public Categorie getCategory() {
        return categorie;
    }

    public void setCategory(Categorie categorie) {
        this.categorie = categorie;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
