package com.ecommerce.modele;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="cos")
public class Cos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(name = "data_creat")
    private Date dataCreat;

    @ManyToOne
    @JoinColumn(name = "produs_id", referencedColumnName = "id")
    private Produs produs;

    @JsonIgnore
    @OneToOne(targetEntity = Utilizator.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "utilizator_id")
    private Utilizator utilizator;


    private int cantitate;

    private String marime;

    public Cos() {
    }

    public Cos(Produs produs, int cantitate, String marime, Utilizator utilizator){
        this.utilizator = utilizator;
        this.produs = produs;
        this.cantitate = cantitate;
        this.marime = marime;
        this.dataCreat = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Utilizator getUser() {
        return utilizator;
    }

    public void setUser(Utilizator utilizator) {
        this.utilizator = utilizator;
    }

    public Date getDataCreat() {
        return dataCreat;
    }

    public void setDataCreat(Date dataCreat) {
        this.dataCreat = dataCreat;
    }

    public Produs getProduct() {
        return produs;
    }

    public void setProduct(Produs product) {
        this.produs = product;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public String getMarime() {
        return marime;
    }

    public void setMarime(String marime) {
        this.marime = marime;
    }
}