package com.ecommerce.dto.comanda;

import javax.validation.constraints.NotNull;

public class DtoArticoleComanda {

    private @NotNull double pret;
    private @NotNull int cantitate;
    private @NotNull int comandaId;
    private @NotNull int idProdus;

    public DtoArticoleComanda() {}

    public DtoArticoleComanda(@NotNull double pret, @NotNull int cantitate, @NotNull int comandaId, @NotNull int idProdus) {
        this.pret = pret;
        this.cantitate = cantitate;
        this.comandaId = comandaId;
        this.idProdus = idProdus;
    }

    public double getPret() {
        return pret;
    }

    public void setPret(double pret) {
        this.pret = pret;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public int getComandaId() {
        return comandaId;
    }

    public void setComandaId(int comandaId) {
        this.comandaId = comandaId;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }
}