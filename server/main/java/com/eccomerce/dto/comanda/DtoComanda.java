package com.ecommerce.dto.comanda;

import com.ecommerce.modele.Order;

import javax.validation.constraints.NotNull;

public class DtoComanda {
    private Integer id;
    private @NotNull Integer utilizatorId;

    public DtoComanda() {
    }

    public DtoComanda(Order order) {
        this.setId(order.getId());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUtilizatorId() {
        return utilizatorId;
    }

    public void setUtilizatorId(Integer utilizatorId) {
        this.utilizatorId = utilizatorId;
    }

}