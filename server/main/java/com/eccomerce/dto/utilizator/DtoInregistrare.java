package com.ecommerce.dto.utilizator;

public class DtoInregistrare {

    private String numeFamilie;
    private String prenume;
    private String email;
    private String parola;

    private boolean permisiuni;

    public boolean isPermisiuni() {
        return permisiuni;
    }

    public void setPermisiuni(boolean permisiuni) {
        this.permisiuni = permisiuni;
    }

    public String getNumeFamilie() {
        return numeFamilie;
    }

    public void setNumeFamilie(String numeFamilie) {
        this.numeFamilie = numeFamilie;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }
}
