package com.ecommerce.dto;

public class DtoRaspuns {

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DtoRaspuns(String status, String message) {
        this.status = status;
        this.message = message;
    }
}
