package com.ecommerce.dto;

import javax.validation.constraints.NotNull;

public class DtoProdus {
    private Integer id;
    private @NotNull String nume;
    private @NotNull String imagineURL;

    private String imagine2URL;

    private String imagine3URL;

    private String imagine4URL;
    private @NotNull double pret;
    private @NotNull String descriere;
    private @NotNull Integer idCategorie;

    private String marimi;

    private String gen;

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public DtoProdus() {
    }

    public String getMarimi() {
        return marimi;
    }

    public void setMarimi(String marimi) {
        this.marimi = marimi;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getImagineURL() {
        return imagineURL;
    }

    public String getImagine2URL() {
        return imagine2URL;
    }

    public void setImagine2URL(String imagine2URL) {
        this.imagine2URL = imagine2URL;
    }

    public String getImagine3URL() {
        return imagine3URL;
    }

    public void setImagine3URL(String imagine3URL) {
        this.imagine3URL = imagine3URL;
    }

    public String getImagine4URL() {
        return imagine4URL;
    }

    public void setImagine4URL(String imagine4URL) {
        this.imagine4URL = imagine4URL;
    }

    public void setImagineURL(String imagineURL) {
        this.imagineURL = imagineURL;
    }

    public double getPret() {
        return pret;
    }

    public void setPret(double pret) {
        this.pret = pret;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public Integer getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(Integer idCategorie) {
        this.idCategorie = idCategorie;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
