package com.ecommerce.dto.cos;

import java.util.List;

public class DtoCos {
    private List<DtoArticolCos> articoleCos;
    private double costTotal;

    public DtoCos(List<DtoArticolCos> dtoArticolCosList, double costTotal) {
        this.articoleCos = dtoArticolCosList;
        this.costTotal = costTotal;
    }

    public List<DtoArticolCos> getcartItems() {
        return articoleCos;
    }

    public void setArticoleCos(List<DtoArticolCos> dtoArticolCosList) {
        this.articoleCos = dtoArticolCosList;
    }

    public double getCostTotal() {
        return costTotal;
    }

    public void setCostTotal(int costTotal) {
        this.costTotal = costTotal;
    }
}