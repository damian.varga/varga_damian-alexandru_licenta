package com.ecommerce.dto.cos;

import javax.validation.constraints.NotNull;

public class DtoAdaugaLaCos {
    private Integer id;
    private @NotNull Integer idProdus;
    private @NotNull Integer cantitate;

    private @NotNull String marime;

    public DtoAdaugaLaCos() {
    }



    @Override
    public String toString() {
        return "CartDto{" +
                "id=" + id +
                ", idProdus=" + idProdus +
                ", cantitate=" + cantitate +
                ", marime=" + marime +
                ",";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(Integer idProdus) {
        this.idProdus = idProdus;
    }

    public Integer getCantitate() {
        return cantitate;
    }

    public void setCantitate(Integer cantitate) {
        this.cantitate = cantitate;
    }

    public String getMarime() {
        return marime;
    }

    public void setMarime(String marime) {
        this.marime = marime;
    }
}