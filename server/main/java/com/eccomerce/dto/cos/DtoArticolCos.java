package com.ecommerce.dto.cos;

import com.ecommerce.modele.Cos;
import com.ecommerce.modele.Produs;

import javax.validation.constraints.NotNull;

public class DtoArticolCos {
    private Integer id;
    private @NotNull Integer cantitate;

    private @NotNull String marime;
    private @NotNull Produs produs;

    public DtoArticolCos() {
    }

    public DtoArticolCos(Cos cos) {
        this.setId(cos.getId());
        this.setCantitate(cos.getCantitate());
        this.setMarime(cos.getMarime());
        this.setProdus(cos.getProduct());
    }

    @Override
    public String toString() {
        return "CartDto{" +
                "id=" + id +
                ", cantitate=" + cantitate +
                ", marime=" + marime +
                ", numeProdus=" + produs.getNume() +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCantitate() {
        return cantitate;
    }

    public void setCantitate(Integer cantitate) {
        this.cantitate = cantitate;
    }

    public String getMarime() {
        return marime;
    }

    public void setMarime(String marime) {
        this.marime = marime;
    }
    public Produs getProdus() {
        return produs;
    }

    public void setProdus(Produs produs) {
        this.produs = produs;
    }

}