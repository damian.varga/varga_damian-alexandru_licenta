package com.ecommerce.dto.checkout;

public class DtoRaspunsStripe {
    private String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public DtoRaspunsStripe(String sessionId) {
        this.sessionId = sessionId;
    }

    public DtoRaspunsStripe() {
    }
}