package com.ecommerce.dto.checkout;

public class DtoArticolCheckout {

    private String numeProdus;
    private int cantitate;
    private double pret;
    private long idProdus;
    private int utilizatorId;

    public DtoArticolCheckout() {}

    public DtoArticolCheckout(String numeProdus, int cantitate, double pret, long idProdus, int utilizatorId) {
        this.numeProdus = numeProdus;
        this.cantitate = cantitate;
        this.pret = pret;
        this.idProdus = idProdus;
        this.utilizatorId = utilizatorId;
    }

    public String getNumeProdus() {
        return numeProdus;
    }

    public void setNumeProdus(String numeProdus) {
        this.numeProdus = numeProdus;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public void setPret(double pret) {
        this.pret = pret;
    }

    public double getPret(){return pret;}

    public int getUtilizatorId() {
        return utilizatorId;
    }

    public void setUtilizatorId(int utilizatorId) {
        this.utilizatorId = utilizatorId;
    }

    public long getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(long id) {
        this.idProdus = id;
    }



}