import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.js'
import swal from 'sweetalert';
window.axios = require('axios')

createApp(App).use(router).mount('#app');

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

